# jplhomer bootstrap

I'm using this to get started with new websites. I use it at work, too. To use it, just fork it!

## Using the Bootstrap

To use the bootstrap, simply fork & clone the repository on your local machine. The development workflow is optimized using [Grunt](http://gruntjs.com), so you'll need to install that (follow the instructions below).

### Installing NodeJS/Grunt

*If you already have NodeJS/Grunt installed, skip to the next step.*

Grunt is a Javascript task runner, which means it runs on the [NodeJS](http://nodejs.org/download/) platform. You'll need to [download NodeJS](http://nodejs.org/download/) to use it. 

You'll then be able to use the Node Package Manager in the command line to download and install Grunt. Run `sudo npm -g install grunt-cli` to globally install Grunt to your system.

### Setting up the project

After cloning this repository, go to its directory on your local machine and open `package.json`. Change the name to the name of your new project.

### Installing Dependencies

Now run `npm install` in the directory. This will grab and install all the dependencies listed in your `package.json` file.

### Grunt

Simply run `grunt` in the command line to do the following: 

 * Concatenate your jQuery, `plugins.js`, and `main.js` files into the new `global.js` file.
 * Uglify `global.js` into `global.min.js`
 * Compile your Sass files with [Compass](http://compass-style.org). In the development environment, the Compass settings are located in `config.rb`.

If you're ready to deploy and want minified CSS, either change the setting in your `config.rb` file or run `grunt dist` to automatically minify them.

### Watching for Changes

By running `grunt watch`, Grunt can watch for any changes to HTML/PHP/JS/CSS files. It can also LiveReload the page in your browser if you have the [Chrome extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en) installed.

## .gitignore

We're ignoring a number of files which don't need to be version-controlled, namely:

 * .DS_Store
 * /node_modules/
 * .sass-cache
 * base.css
 * fallback.css
 * js/global.min.js

## Changelog

 * 09-16-2013	v1.0.2    Updated Flexslider to 2.2.0 and fixed a paragraph margin-bottom error.
 * 08-21-2013   v1.0.1    Added the .media object
 * 08-13-2013	v1.0.0    Completely overhaul and implement GruntJS into workflow.
 * 08-13-2013	v0.9.0    Added Wordpress standard assistive text class to `_reset.scss`. Added `.htaccess` with standard caching techniques.
 * 				v0.8.0    Added encoding to `config.rb`. Removed `main.css` and `fallback.css` from tracking.
 *				v0.7.0    Added a placeholder JS solution (for older IE browsers) in the case of using the input `placeholder` attribute as your primary label.
 * 				v0.6.0    Got rid of the 75em width (going to limit to 940px max width for now). Moved to the default Compass mixins instead of the sheet of mixins (more available).
 *				v0.5.0    Moved away from rasterizing everything with pixels. Using em now instead. 16px = body default (100%) font size = 1em.
 *				v0.4.0    Added Flexslider as a default. Use it in almost every project! Removed unused SCSS variables.
 *				v0.3.0    Added "width: auto" to responsive images, took away the unused min-width style. Added a bunch of meta tags.
 *				v0.2.0    Fixed viewport issue.
 *				v0.1.0    Initial commit. Included SASS-based stylesheets, basic JS framework, and an images folder.