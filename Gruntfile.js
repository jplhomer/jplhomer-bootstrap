module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/**\n' +
            '* <%= pkg.name %>.js v<%= pkg.version %> by @jplhomer\n' +
            '* Copyright <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            '*/\n',
    compass: {
      dev: {
        options: {
          config: 'config.rb'
        }
      },
      dist: {
        options: {
          sassDir: 'sass',
          cssDir: 'css',
          environment: 'production'
        }
      }
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: false
      },
      global: {
        src: ['js/vendor/jquery-1.8.0.min.js','js/src/plugins.js', 'js/src/main.js'],
        dest: 'js/global.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      global: {
        files: {
          'js/global.min.js': ['<%= concat.global.dest %>']
        }
      }
    },
    watch : {
      sass: {
        files: ['sass/*.scss'],
        tasks: ['compass:dev']
      },
      /* watch and see if our javascript files change, or new packages are installed */
      js: {
        files: ['js/src/plugins.js', 'js/src/main.js'],
        tasks: ['concat', 'uglify']
      },
      /* watch our files for change, reload */
      livereload: {
        files: ['*.html', '*.php', 'css/*.css', 'img/*', 'js/global.js'],
        options: {
          livereload: true
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-compass');

  grunt.registerTask('dist', ['concat', 'uglify', 'compass:dist']);

  // Default task(s).
  grunt.registerTask('default', ['concat', 'uglify', 'compass:dev']);

};